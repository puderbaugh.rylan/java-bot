package me.lex;

import me.lex.commands.Shutdown;
import me.lex.commands.*;
import me.lex.listeners.Filter;
import me.lex.listeners.Startup;
import me.lex.listeners.VoiceConnectionLogs;
import me.lex.listeners.Welcome;
import me.lex.model.BotConfig;
import me.lex.repository.BotConfigFileRepository;
import me.lex.repository.BotConfigRepository;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.utils.MemberCachePolicy;
import net.dv8tion.jda.api.utils.cache.CacheFlag;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.Scanner;

public class Bot {
    private static final BotConfigRepository botConfigRepository = new BotConfigFileRepository();

    public static void main(String[] args) throws IOException {
        String botToken = botConfigRepository.fetch().token();

        if (!botToken.equals("token")) {
            exec(botToken);
        } else {
            updateToken();
            exec(botConfigRepository.fetch().token());
        }
    }

    public static void exec(String botToken) {
        JDABuilder.createLight(botToken)
                .addEventListeners(new CommandManager())

                // Commands
                .addEventListeners(new Help())
                .addEventListeners(new Ping())
                .addEventListeners(new SetupVerify())
                .addEventListeners(new Shutdown())

                // Listeners
                .addEventListeners(new Welcome())
                .addEventListeners(new VoiceConnectionLogs())
                .addEventListeners(new Filter())
                .addEventListeners(new Startup())

                .enableCache(CacheFlag.VOICE_STATE)

                .setMemberCachePolicy(MemberCachePolicy.ALL)
                .enableIntents(GatewayIntent.MESSAGE_CONTENT, GatewayIntent.GUILD_MEMBERS, GatewayIntent.GUILD_VOICE_STATES)
                .build();
    }

    private static void updateToken() throws IOException {
        try {
            String tokenInput = JOptionPane.showInputDialog("Provide Bot Token:");

            if (tokenInput != null) {
                BotConfig existingConfig = botConfigRepository.fetch();
                botConfigRepository.save(new BotConfig(
                        tokenInput,
                        existingConfig.ownerId(),
                        existingConfig.welcomeChannelId(),
                        existingConfig.logsChannelId(),
                        existingConfig.ignoredUsersId(),
                        existingConfig.verifyChannelId(),
                        existingConfig.verifyRoleId(),
                        existingConfig.blacklistedWords(),
                        existingConfig.rules()
                ));
            } else {
                System.exit(0);
            }
        } catch (HeadlessException ex) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("No X11 DISPLAY variable was set, using Headless.");

            System.out.print("Enter your bots token: ");
            String tokenInput = scanner.next();

            BotConfig existingConfig = botConfigRepository.fetch();
            botConfigRepository.save(new BotConfig(
                    tokenInput,
                    existingConfig.ownerId(),
                    existingConfig.welcomeChannelId(),
                    existingConfig.logsChannelId(),
                    existingConfig.ignoredUsersId(),
                    existingConfig.verifyChannelId(),
                    existingConfig.verifyRoleId(),
                    existingConfig.blacklistedWords(),
                    existingConfig.rules()
            ));
        }
    }
}

