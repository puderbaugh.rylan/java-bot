package me.lex.listeners;

import me.lex.repository.BotConfigFileRepository;
import me.lex.repository.BotConfigRepository;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.io.IOException;
import java.util.Arrays;

public class Filter extends ListenerAdapter {
    BotConfigRepository botConfigRepository = new BotConfigFileRepository();

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        if (!(event.getMessage().getAuthor() == event.getJDA().getSelfUser())) {
            String[] messageSent = event.getMessage().getContentRaw().split(" ");
            String[] blacklistedWords;
            Long logsChannelID;
            User user = event.getAuthor();
            String logMessage;

            try {
                blacklistedWords = botConfigRepository.fetch().blacklistedWords();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            for (String word : messageSent) {
                logMessage = user.getAsMention() + " has been warned for saying **" + word + "** in the message **" + Arrays.toString(messageSent) + "**";

                for (String blacklistedWord : blacklistedWords) {
                    if (word.contains(blacklistedWord)) {
                        event.getMessage().delete().queue();
                        event.getAuthor().openPrivateChannel().flatMap(channel -> channel.sendMessage(
                                "Please refrain from saying " + "'**" + word + "**'" + " in the server " + "**" + event.getGuild().getName() + "**")).queue();
                        try {
                            logsChannelID = botConfigRepository.fetch().logsChannelId();
                            event.getGuild().getTextChannelById(String.valueOf(logsChannelID)).sendMessage(logMessage).queue();

                            /*if (logsChannelID == null) {
                                // TODO send message to owner mentioning invalid ID
                                event.getGuild().getMemberById(botConfigRepository.fetch().ownerId()).getUser().openPrivateChannel()
                                        .flatMap(channel -> channel.sendMessage("Please provide a valid verifyRoleId in your config.json file.")).queue();
                            }*/
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    }
                }
            }
        }
    }
}
