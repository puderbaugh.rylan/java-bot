package me.lex.listeners;

import me.lex.repository.BotConfigFileRepository;
import me.lex.repository.BotConfigRepository;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.channel.concrete.TextChannel;
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.io.IOException;

public class Welcome extends ListenerAdapter {
    BotConfigRepository botConfigRepository = new BotConfigFileRepository();

    @Override
    public void onGuildMemberJoin(GuildMemberJoinEvent event) {
        TextChannel welcomeChannel;
        Long verifyChannel;
        Guild guild = event.getGuild();
        String serverName = guild.getName();
        String userMention = event.getMember().getAsMention();
        String welcomeMessage;

        try {
            welcomeChannel = event.getJDA().getTextChannelById(botConfigRepository.fetch().welcomeChannelId());
            if(welcomeChannel == null) {
                // TODO send message to owner mentioning invalid ID
                /*event.getGuild().getMemberById(botConfigRepository.fetch().ownerId()).getUser().openPrivateChannel()
                        .flatMap(channel -> channel.sendMessage("Please provide a valid welcomeChannelId in your config.json file.")).queue();*/
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        try {
            verifyChannel = botConfigRepository.fetch().verifyChannelId();
            welcomeMessage = "Welcome to **" + serverName + "** " + userMention +
                    ". To gain access to the server verify in " +
                    event.getGuild().getTextChannelById(String.valueOf(verifyChannel)).getAsMention();

            /*if(verifyChannel == null) {
                // TODO send message to owner mentioning invalid ID
                event.getGuild().getMemberById(botConfigRepository.fetch().ownerId()).getUser().openPrivateChannel()
                        .flatMap(channel -> channel.sendMessage("Please provide a valid verifyRoleId in your config.json file.")).queue();
            }*/
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        guild.getTextChannelById(welcomeChannel.getId()).sendMessage(welcomeMessage).queue();
    }
}