package me.lex.listeners;

import net.dv8tion.jda.api.events.session.ReadyEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class Startup extends ListenerAdapter {
    @Override
    public void onReady(ReadyEvent event) {
        String botName = event.getJDA().getSelfUser().getName();
        System.out.println(botName + " is now online!");
    }
}