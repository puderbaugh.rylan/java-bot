package me.lex.listeners;

import me.lex.repository.BotConfigFileRepository;
import me.lex.repository.BotConfigRepository;
import net.dv8tion.jda.api.entities.channel.concrete.TextChannel;
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceUpdateEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

public class VoiceConnectionLogs extends ListenerAdapter {
    BotConfigRepository botConfigRepository = new BotConfigFileRepository();

    public String getTime() {
        Date date = new Date();
        SimpleDateFormat df = new SimpleDateFormat("[ MM/dd - hh:mm aaa ] ");
        return df.format(date.getTime());
    }

    @Override
    public void onGuildVoiceUpdate(GuildVoiceUpdateEvent event) {
        String userMention = event.getMember().getAsMention();
        String userName = event.getMember().getUser().getName();

        long[] ignoredUsersId;
        try {
            ignoredUsersId = botConfigRepository.fetch().ignoredUsersId();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }


        List<Long> ignoredUsers = new ArrayList<>(ignoredUsersId.length);
        for (long ignoredUser : ignoredUsersId) {
            ignoredUsers.add(ignoredUser);
        }

        TextChannel channel;
        try {
            channel = event.getJDA().getTextChannelById(botConfigRepository.fetch().logsChannelId());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        for (long ignoredUser : ignoredUsers) {
            boolean isIgnoredUser = ignoredUsers.contains(event.getMember().getIdLong());

            if (isIgnoredUser && channel != null) {
                if (event.getNewValue() == event.getChannelJoined() && event.getChannelJoined() != null) {
                    String channelJoined = event.getChannelJoined().getAsMention();
                    channel.sendMessage(getTime() + " " + userName + " has joined the voice channel " + channelJoined).queue();

                } else if (event.getNewValue() == null) {
                    String channelLeft = event.getChannelLeft().getAsMention();
                    channel.sendMessage(getTime() + " " + userName + " has left the voice channel " + channelLeft).queue();
                }
                break;

            } else if (event.getNewValue() == event.getChannelJoined() && event.getChannelJoined() != null) {
                String channelJoined = event.getChannelJoined().getAsMention();
                channel.sendMessage(getTime() + " " + userMention + " has joined the voice channel " + channelJoined).queue();

            } else if (event.getNewValue() == null) {
                String channelLeft = event.getChannelLeft().getAsMention();
                channel.sendMessage(getTime() + " " + userMention + " has left the voice channel " + channelLeft).queue();
            }
            break;
        } /*else if (channel == null) {
                // TODO send message to owner mentioning invalid ID
            }*/
    }
}