package me.lex.commands;

import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;

public class Ping extends ListenerAdapter {
    @Override
    public void onSlashCommandInteraction(@NotNull SlashCommandInteractionEvent event) {
        String command = event.getName();

        if (command.equals("ping")) {
            long time = System.currentTimeMillis();
            event.reply("Retrieving Latency..").setEphemeral(true)
                    .flatMap(v -> event.getHook().editOriginalFormat("Latency: %d ms", System.currentTimeMillis() - time)).queue();
        }
    }
}