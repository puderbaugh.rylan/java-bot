package me.lex.commands;

import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class Shutdown extends ListenerAdapter {
    @Override
    public void onSlashCommandInteraction(SlashCommandInteractionEvent event) {
        String command = event.getName();
        Member member = event.getMember();

        if (command.equals("shutdown") && member.isOwner()) {
            event.reply("Shutting down..").setEphemeral(true).queue();

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            event.getJDA().shutdown();
            System.exit(0);
        } else if (command.equals("shutdown")) {
            event.reply("Only the owner can use this command!").setEphemeral(true).queue();
        }
    }
}