package me.lex.commands;

import me.lex.model.BotConfig;
import me.lex.repository.BotConfigFileRepository;
import me.lex.repository.BotConfigRepository;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.UserSnowflake;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.events.interaction.component.ButtonInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.interactions.components.buttons.Button;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

public class SetupVerify extends ListenerAdapter {
    BotConfigRepository botConfigRepository = new BotConfigFileRepository();

    @Override
    public void onSlashCommandInteraction(@NotNull SlashCommandInteractionEvent event) {
        String command = event.getName();
        Member member = event.getMember();

        if (command.equals("setup-verify") && member.isOwner()) {
            long textChannelId = event.getChannel().asTextChannel().getIdLong();
            Role role = event.getGuild().getRoleById(event.getOption("role-id").getAsString());

            EmbedBuilder embed = new EmbedBuilder();
            String verify = "If you agree to abide by these rules,  react to this message with the blue check mark.";

            embed.setTitle("Rules");

            try {
                String rules = rulesToString();
                embed.setDescription(rules + "\n" + verify);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            try {
                if(role != null) {
                    updateVerifyConfigValues(textChannelId, role.getIdLong());
                    event.getChannel().sendMessageEmbeds(embed.build())
                            .addActionRow(Button.primary("verify", "Verify")).queue();
                } else {
                    event.getGuild().getOwner().getUser().openPrivateChannel().flatMap(channel -> channel.sendMessage("Please provide a valid role-id when using the command **setup-verify!**")).queue();
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

        } else if (command.equals("setup-verify")) {
            event.reply("Only the owner can use this command!").setEphemeral(true).queue();
        }
    }

    @Override
    public void onButtonInteraction(@NotNull ButtonInteractionEvent event) {
        Member bot = event.getGuild().getSelfMember();
        Member member = event.getMember();

        if (event.getComponentId().equals("verify")) {
            Role role;
            try {
                role = event.getJDA().getRoleById(botConfigRepository.fetch().verifyRoleId());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            long userId = event.getMember().getIdLong();

            if(role !=null && bot.canInteract(role)) {
                event.getJDA().getGuildById(event.getGuild().getId()).addRoleToMember(UserSnowflake.fromId(userId), role).queue();

                if(member.getGuild().getRoles().contains(role)) {
                    event.reply("Successfully Verified!").setEphemeral(true).queue();
                } else {
                    event.reply("Role was not added for some reason!").setEphemeral(true).queue();
                }
            } else if(role == null) {
                event.reply("Invalid role ID has been provided the config.json file!").setEphemeral(true).queue();
                event.getGuild().getOwner().getUser().openPrivateChannel().flatMap(channel -> channel.sendMessage("Please provide a valid verifyRoleId in your config.json file.")).queue();
            } else {
                event.reply("The bots role needs to be higher than your current role!").setEphemeral(true).queue();
            }
        }
    }

    private void updateVerifyConfigValues(long textChannelId , long roleId) throws IOException {
        BotConfig existingConfig = botConfigRepository.fetch();
        botConfigRepository.save(new BotConfig(
                existingConfig.token(),
                existingConfig.ownerId(),
                existingConfig.welcomeChannelId(),
                existingConfig.logsChannelId(),
                existingConfig.ignoredUsersId(),
                textChannelId,
                roleId,
                existingConfig.blacklistedWords(),
                existingConfig.rules()
        ));
    }

    private String rulesToString() throws IOException {
        StringBuilder rules = new StringBuilder();

        String[] ruleList = botConfigRepository.fetch().rules();

        for(String rule : ruleList) {
            rules.append(rule);
            rules.append(System.lineSeparator());
        }

        return rules.toString();
    }
}
