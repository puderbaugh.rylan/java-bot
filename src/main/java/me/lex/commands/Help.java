package me.lex.commands;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;

public class Help extends ListenerAdapter {
    @Override
    public void onSlashCommandInteraction(@NotNull SlashCommandInteractionEvent event) {
        String command = event.getName();
        String botName = event.getJDA().getSelfUser().getName();

        if (command.equals("help")) {
            EmbedBuilder embed = new EmbedBuilder();
            embed.setTitle(botName + " Commands");
            embed.setDescription(
                    """
                            User
                            -----
                            help
                            ping

                            Owner
                            --------
                            setup-verify
                            shutdown"""
            );

            event.replyEmbeds(embed.build()).setEphemeral(true).queue();
        }
    }
}
