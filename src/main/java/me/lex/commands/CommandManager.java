package me.lex.commands;

import net.dv8tion.jda.api.events.session.ReadyEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class CommandManager extends ListenerAdapter {
    /**
     * Registers slash commands as GUILD commands (max 100).
     * These commands will update instantly and are great for testing.
     */
    /*@Override
    public void onGuildReady(@NotNull GuildReadyEvent event) {
        List<CommandData> commandData = new ArrayList<>();
        commandData.add(Commands.slash("help", "Display all available commands."));
        commandData.add(Commands.slash("ping", "Calculate the latency of the bot."));
        commandData.add(Commands.slash("setup-verify", "Display all available commands.").addOption(OptionType.STRING, "role-id", "Role ID to assign the user when verified."));
        commandData.add(Commands.slash("shutdown", "Gracefully shutdown the bot."));
        event.getGuild().updateCommands().addCommands(commandData).queue();
    }*/

    /**
     * Registers slash commands as GLOBAL commands (unlimited).
     * These commands may take up to an hour to update.
     */
    @Override
    public void onReady(@NotNull ReadyEvent event) {
        List<CommandData> commandData = new ArrayList<>();
        commandData.add(Commands.slash("help", "Display all available commands."));
        commandData.add(Commands.slash("ping", "Calculate the latency of the bot."));
        commandData.add(Commands.slash("setup-verify", "Display a verification method to verify users.").addOption(OptionType.STRING, "role-id", "Role ID to assign the user when verified."));
        commandData.add(Commands.slash("shutdown", "Gracefully shutdown the bot."));
        event.getJDA().updateCommands().addCommands(commandData).queue();
    }
}
