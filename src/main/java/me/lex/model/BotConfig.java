package me.lex.model;

public record BotConfig(
        String token,
        long ownerId,
        long welcomeChannelId,
        long logsChannelId,
        long[] ignoredUsersId,
        long verifyChannelId,
        long verifyRoleId,
        String[] blacklistedWords,
        String[] rules
) {
}