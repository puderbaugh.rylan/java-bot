package me.lex.repository;

import me.lex.model.BotConfig;

import java.io.IOException;

public interface BotConfigRepository {
    void save(BotConfig botConfig) throws IOException;
    void delete() throws IOException;
    BotConfig fetch() throws IOException;
}