package me.lex.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import me.lex.model.BotConfig;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

public class BotConfigFileRepository implements BotConfigRepository{
    private final ObjectMapper jsonMapper = new ObjectMapper();

    @Override
    public void save(BotConfig botConfig) throws IOException {
        Path filePath = getFilePath();

        String json = jsonMapper.writerWithDefaultPrettyPrinter().writeValueAsString(botConfig);

        System.out.println("Saving: " + json + " to " + filePath);

        delete();

        Files.writeString(filePath, json,
                StandardCharsets.UTF_8, StandardOpenOption.CREATE);
    }

    @Override
    public void delete() throws IOException {
        Path filePath = getFilePath();

        if (filePath.toFile().exists()) {
            Files.delete(filePath);
        }
    }

    @Override
    public BotConfig fetch() throws IOException {
        Path filePath = getFilePath();

        if(!filePath.toFile().exists()) {
            BotConfig botConfig = new BotConfig(
                    "token",
                    0L,
                    1L,
                    2L,
                    new long[]{0L, 1L},
                    3L,
                    4L,
                    new String[]{"loser", "dummy"},
                    new String[]{"Use common sense", "Be respectful"}
            );

            save(botConfig);
        }

        return jsonMapper.readValue(filePath.toFile(), BotConfig.class);
    }

    public Path getFilePath() {
        return Path.of("", "config.json");
    }
}